# !/usr/bin/env python3
import argparse
import glob
import time
from collections import defaultdict

import requests

from dependator.map import Mapper
from migrator.migrator import *
from repomanager.repoman import *

if __name__ == "__main__":
    start_time = time.time()
    parser = argparse.ArgumentParser(description='Migrate OCA repositories to Flectra Community')
    parser.add_argument('--src', default='./OCA', help='Path to OCA root')
    parser.add_argument('--dest', default='./FC', help='Path to Flectra root')
    parser.add_argument('--no-create', default=False, action='store_true', help='Do not create repositories at gitlab')
    parser.add_argument('--no-commit', default=False, action='store_true', help='Do not commit changes')
    parser.add_argument('--no-push', default=False, action='store_true', help='Do not push changes')
    parser.add_argument('--no-oca', default=False, action='store_true', help='Ignore OCA repositories')
    parser.add_argument('--debug', default=False, action='store_true', help='Loglevel: DEBUG')
    parser.add_argument('--ignore-limit', default=True, action='store_true', help='Do not stop after 20 repositories')
    parser.add_argument('--single-repo', default=[], action='append', help='Repository name e.g. partner-contact')
    parser.add_argument('--private-token', help='Gitlab token')
    args = parser.parse_args()
    if args.private_token:
        private_token = args.private_token
    else:
        private_token = os.environ['GITLAB_PRIVATE_TOKEN']

    log_level = logging.INFO
    if args.debug:
        log_level = logging.DEBUG

    logging.basicConfig(
            level=log_level,
            format='%(asctime)s %(levelname)-8s %(message)s',
            datefmt='%a, %d %b %Y %H:%M:%S'
    )
    _logger = logging.getLogger(__name__)
    _logger.info('Starting module migration')

    if os.path.exists(args.src):
        _logger.debug('Removing source folder %s', args.src)
        shutil.rmtree(args.src)
    _logger.debug('Creating source folder %s', args.src)
    os.mkdir(args.src)

    if os.path.exists(args.dest):
        _logger.debug('Removing destination folder %s', args.dest)
        shutil.rmtree(args.dest)
    _logger.debug('Creating destination folder %s', args.dest)
    os.mkdir(args.dest)

    gh = GitHub(args.src, args.single_repo, args.no_oca)

    gl = GitLab(args.dest, private_token, not args.debug, args.single_repo)
    gl.update_project_settings()

    dependency_graph = Mapper()

    migrators = []

    change_repos_count = 0

    for name, url in sorted(gh.repositories.items()):
        new_name = gl.get_new_name(name)
        repo_src_path = os.path.join(os.path.abspath(args.src), name)
        repo_dst_path = os.path.join(os.path.abspath(args.dest), new_name)

        gh_repo = gh.clone(name, url)

        skip_repo = False
        if not gh_repo:
            skip_repo = True
            _logger.info('cloning OCA repository %s not possible because of no branch', name)

        if not skip_repo and not glob.glob(repo_src_path + '/*/__manifest__.py'):
            _logger.info('No modules found in repository %s, skipping it', name)
            skip_repo = True

        if skip_repo:
            gl.delete(name)
            continue

        if not gl.get_repository(new_name):
            _logger.info('No flectra community repository found for %s, creating it', new_name)

            if not gl.create(new_name):
                _logger.error('Could not create repository for %s, skipping', new_name)
                continue

        try:
            gl.clone(new_name)
        except Exception as ex:
            _logger.error('Clone failed for repository %s: %s', new_name, str(ex))
            if args.debug:
                raise
            continue

        remote = gl.get_repository(new_name)
        migrator = RepoMigrator(new_name, repo_src_path, repo_dst_path, remote, dependency_graph)
        migrators.append(migrator)

    for migrator in migrators:
        try:
            migrator.migrate()
        except Exception as ex:
            _logger.error('Unknown error occured while migration repository %s: %s', migrator.name, str(ex))
            if args.debug:
                raise

    for migrator in migrators:
        try:
            migrator.generate_gitlab_ci()
            migrator.local_repo.add_all()
            if migrator.local_repo.has_changes:
                change_repos_count += 1
                _logger.info('Changes at repository %s detected', migrator.name)
                if not args.no_commit:
                    migrator.local_repo.commit_changes()

            if not migrator.local_repo.has_fix_branch:
                if not args.no_commit:
                    migrator.local_repo.create_fixed_branch()

            if not args.no_commit:
                migrator.local_repo.merge_upstream_to_fixed()

            if not args.no_commit and not args.no_push:
                migrator.local_repo.push_changes()
                if migrator.remote['has_master_branch']:
                    gl.create_merge_request(migrator.remote)
                migrator.cleanup()

        except Exception as ex:
            _logger.error('Unknown error occured while completing repository %s: %s', migrator.name, str(ex))
            if args.debug:
                raise

        if change_repos_count > 20 and not args.ignore_limit:
            _logger.warning('More than 20 changed repositories found. '
                            'Stopping current run for not overheating gitlab runners.')
            break

    repo_dependencies = defaultdict(dict)
    for migrator in migrators:
        repo_dependencies[migrator.name] = {
            'depends_on': [],
            'used_by': [],
        }
        for dep in migrator.dependencies:
            repo_dependencies[migrator.name]['depends_on'].append(dep)

    for migrator in migrators:
        for dep in migrator.dependencies:
            repo_dependencies[dep]['used_by'].append(migrator.name)

    module_dependencies = defaultdict(dict)
    for migrator in migrators:
        if not migrator.modules:
            continue
        for name, module in migrator.modules.items():
            module_dependencies[name] = {
                'repo': migrator.name,
                'depends_on': [],
                'used_by': [],
            }
            if module and 'depends' in module:
                for dep in module['depends']:
                    module_dependencies[name]['depends_on'].append(dep)

    for migrator in migrators:
        if not migrator.modules:
            continue
        for name, module in migrator.modules.items():
            if module and 'depends' in module:
                for dep in module['depends']:
                    if dep not in module_dependencies:
                        module_dependencies[dep] = {
                            'repo': False,
                            'depends_on': [],
                            'used_by': [],
                        }
                    module_dependencies[dep]['used_by'].append(name)

    server_url = "https://flectra-community.org"
    _logger.info('Sending repository dependencies to %s' % server_url)
    requests.post(server_url + "/gitlab/dependencies/repo/FlectraCommunityRepoUpdate", json=repo_dependencies)
    _logger.info('Sending module dependencies to %s' % server_url)
    requests.post(server_url + "/gitlab/dependencies/mods/FlectraCommunityRepoUpdate", json=module_dependencies)
    dependency_graph.show_tree()

    _logger.info('Module migration finished')
    _logger.info('Migration took {0} second'.format(time.time() - start_time))
