import argparse

from repomanager.repoman import *

if __name__ == "__main__":

    logging.basicConfig(
            level='DEBUG',
            format='%(asctime)s %(levelname)-8s %(message)s',
            datefmt='%a, %d %b %Y %H:%M:%S'
    )
    _logger = logging.getLogger(__name__)
    _logger.info('Starting Repository read')
    parser = argparse.ArgumentParser(description='Repository Tools')
    parser.add_argument('--private-token', help='Gitlab token')
    args = parser.parse_args()

    gl = GitLab('.', args.private_token, read_hooks=False)

    for name, data in gl.repositories.items():
        if data['has_master_branch']:
            print('  - name:', name)
            print('    URL:', data['ssh_url_to_repo'])
            print("    type: 'multi'")
            print("    folder: 'flecom'")
            print("    tags: ['basis']")
