import argparse
import logging
from urllib import request

from repomanager.repoman import GitLab

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Migrate OCA repositories to Flectra Community')
    parser.add_argument('--src', default='./OCA', help='Path to OCA root')
    parser.add_argument('--dest', default='./FC', help='Path to Flectra root')
    parser.add_argument('--update-key', default=False, action='store_true', help='Update gitlab deploy keys')
    parser.add_argument('--no-create', default=False, action='store_true', help='Do not create repositories at gitlab')
    parser.add_argument('--no-commit', default=False, action='store_true', help='Do not commit changes')
    parser.add_argument('--no-push', default=False, action='store_true', help='Do not push changes')
    parser.add_argument('--debug', default=False, action='store_true', help='Loglevel: DEBUG')
    parser.add_argument('--single-repo', default=[], action='append', help='Repository name e.g. partner-contact')
    parser.add_argument('--private-token', help='Gitlab token')
    parser.add_argument('--private-token-delete', help='Gitlab token')
    args = parser.parse_args()

    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s %(levelname)-8s %(message)s',
        datefmt='%a, %d %b %Y %H:%M:%S'
    )
    _logger = logging.getLogger(__name__)
    _logger.info('Starting module migration')

    gl = GitLab(args.dest, args.private_token)
    fc_repos = gl.get_pipelines(args.single_repo)

    running = []
    success = []
    warning = []
    error = []
    for repo, data in fc_repos.items():
        if data['status'] == 'failed':
            if data['yaml_errors']:
                error.append('%s: Pipeline failed with YAML errors (%s)' % (repo, data['web_url']))
            else:
                warning.append('%s: Pipeline failed (%s)' % (repo, data['web_url']))
        elif data['status'] == 'success':
            success.append('%s: Pipeline successful finished (%s)' % (repo, data['web_url']))
        elif data['status'] in ['running', 'pending']:
            running.append('%s: Pipeline is running (%s)' % (repo, data['web_url']))

    _logger.info('-----------------------------------------------------')
    _logger.info('Total %s running/pending pipelines', len(running))
    _logger.info('-----------------------------------------------------')
    for run in running:
        _logger.info(run)

    _logger.info('-----------------------------------------------------')
    _logger.info('Total %s successful pipelines', len(success))
    _logger.info('-----------------------------------------------------')
    for sucs in success:
        _logger.info(sucs)

    _logger.info('-----------------------------------------------------')
    _logger.info('Total %s failed pipelines', len(warning))
    _logger.info('-----------------------------------------------------')
    for warn in warning:
        _logger.warning(warn)

    _logger.info('-----------------------------------------------------')
    _logger.info('Total %s failed pipelines with YAML errors', len(error))
    _logger.info('-----------------------------------------------------')
    for err in error:
        _logger.error(err)
