from collections import defaultdict

import networkx as nx

can_show = True
try:
    import matplotlib.pyplot as plt
except ImportError:
    can_show = False


class Mapper(object):
    _module_graph = False

    _modules = False

    def __init__(self):
        self._module_graph = nx.DiGraph()
        self._repo_graph = nx.DiGraph()
        self._modules = defaultdict(list)
        self._module_repos = defaultdict(str)
        self._graph_generated = False

    def add_module(self, module, depends_on, repo):
        self._modules[module].append(depends_on)
        self._module_repos[module] = repo

    def _generate_graph(self):
        if self._graph_generated:
            return

        for mod, dependencies in self._modules.items():
            for dep in dependencies:
                if dep in self._modules:
                    if not self._module_graph.has_node(mod):
                        self._module_graph.add_node(mod)
                    if not self._module_graph.has_node(dep):
                        self._module_graph.add_node(dep)
                    self._module_graph.add_edge(mod, dep)

                    mod_repo = self._module_repos[mod]
                    if not self._repo_graph.has_node(mod_repo):
                        self._repo_graph.add_node(mod_repo)

                    dep_repo = self._module_repos[dep]
                    if not self._repo_graph.has_node(dep_repo):
                        self._repo_graph.add_node(dep_repo)

                    if not self._repo_graph.has_edge(mod_repo, dep_repo):
                        self._repo_graph.add_edge(mod_repo, dep_repo)

        self._graph_generated = True

    def get_depending_repos(self, repo):
        self._generate_graph()

        try:
            result = [entry for entry in list(nx.dfs_preorder_nodes(self._repo_graph, repo)) if entry != repo]
        except:
            return []

        return sorted(result)

    def get_depending_modules(self, module):
        self._generate_graph()

        try:
            result = [entry for entry in list(nx.dfs_preorder_nodes(self._module_graph, module)) if entry != module]
        except:
            return []

        return sorted(result)

    def show_tree(self):
        if not can_show:
            return
        nx.nx_pydot.write_dot(self._repo_graph, 'test.dot')
        nx.nx_agraph.write_dot(self._repo_graph, 'test2.dot')
        plt.title('Test')
        pos = nx.nx_agraph.graphviz_layout(self._repo_graph, prog='sfdp')
        nx.draw(self._repo_graph, pos, with_labels=True)
        plt.show()

    def show(self):
        if not can_show:
            return

        plt.figure(figsize=(11.69, 16.53))
        plt.subplot(2, 1, 1)
        repo_colors = {}
        repo_node_colors = []
        module_node_colors = []
        base_color = 1
        for node in self._repo_graph.nodes:
            repo_colors[node] = base_color
            repo_node_colors.append(base_color)
            base_color += 1

        for node in self._module_graph.nodes:
            module_node_colors.append(repo_colors[self._module_repos[node]])

        nx.draw_shell(self._module_graph,
                      with_labels=True,
                      font_size=8,
                      node_size=150,
                      cmap='tab20c',
                      node_color=module_node_colors,
                      edge_color='#AAAAAA')
        plt.subplot(2, 1, 2)
        nx.draw_shell(self._repo_graph,
                      with_labels=True,
                      font_size=8,
                      node_size=300,
                      cmap='tab20c',
                      node_color=repo_node_colors,
                      edge_color='#AAAAAA')
        plt.show()
